using UnityEngine;
public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public Camera CamaraFP;
    public GameObject proyectil;
    public int vidaplayer;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        //vida del player//
        if (vidaplayer <= 0)
        {
            Debug.Log("GAME OVER");
        }



        //MOVIMIENTO DEL JUGADOR//
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }



        //RAY Y DISPARAR PROYECTILES//
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = CamaraFP.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(CamaraFP.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro, 3);




            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }
        }




        //DA�O DEL ENEMIGO//
        void OnTriggerEnter(Collider coll)
        {
            if (coll.CompareTag("arma"))
            {
                print("da�o");
            }
        }


    }
}