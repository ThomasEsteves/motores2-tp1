using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Transform Target;
    public GameObject Jugador;
   
    private void OnTriggerEnter(Collider other)
    {
        Jugador.transform.position = Target.transform.position;
    }
}
